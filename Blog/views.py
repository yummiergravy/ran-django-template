from django.shortcuts import render, redirect
from myauth.models import OUser
from django.contrib.auth.decorators import login_required
from django.contrib import messages
import requests

def get_main(request):
        return render(request, 'home.html')

def get_main_flash(request, type):
    #errors
    if type == "erroruseraccess":
        messages.error(request, 'You do not have access to this. Please login, or stop trying to go here!')
        return redirect('main')
    if type == "discordnotver":
        messages.error(request, 'Discord account not verified! Verify discord with email, then try again!')
        return redirect('main')
    #successes
    if type == "loginsuccessful":
        messages.success(request, 'You have been logged in!')
        return redirect('main')
    if type == "signupsuccessful":
        messages.success(request, 'You have been signed up!')
        return redirect('main')
    if type == "logoutsuccessful":
        messages.success(request, 'You have been logged out!')
        return redirect('main')


def get_user_profile(request, username):
    if request.user.is_authenticated:
        user = OUser.get_handle
        return render(request, 'user_profile.html', {"user":user})
    else:
        return redirect('flash/erroruseraccess')

def handler404(request, exception):
    messages.error(request, '404, Page not Found!')
    return redirect('main')


def handler500(request):
    messages.error(request, '500 Error!')
    return redirect('main')








