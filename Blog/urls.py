"""Blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.views.generic.base import RedirectView
from django.conf.urls import url
from django.urls import path, include
from .views import *
from django.conf.urls.static import static
from django.conf import settings
from django.views.static import serve
from django.views.generic.base import TemplateView
from . import views

handler404 = 'views.handler404'


urlpatterns = [
    path(r'admin/', admin.site.urls),
    url(r'^$', views.get_main,name='main'),
    url(r'flash/(?P<type>[a-zA-Z0-9]+)$', views.get_main_flash,name='flash_main'),
    url(r'about/', TemplateView.as_view(template_name='about.html'),name='about'),
    url(r'profile/(?P<username>[a-zA-Z0-9]+)$', views.get_user_profile,name="user_profile"),
    path(r'accounts/', include('myauth.urls')),
    path('summernote/', include('django_summernote.urls')),
    path('jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS

    url(r'^favicon.ico$', RedirectView.as_view(url=r'media/favicon.ico')),
    path(r'iot/', include('apps.myapp.urls')),
    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT,) + static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
