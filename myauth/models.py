from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.

class OUser(AbstractUser):
    ouser_id = models.CharField(max_length=25, null=True)
    ouser_avatar = models.CharField(max_length=100, null=True)
    ouser_locale = models.CharField(max_length=10, null=True)
    ouser_handle = models.CharField(max_length=50, null=True)
    ouser_discriminator = models.CharField(max_length=5, null=True)
    ouser_ver = models.BooleanField(null=True)

    def get_id(self):
        return self.ouser_id

    def get_avatar(self):
        return self.ouser_avatar

    def get_locale(self):
        return self.ouser_locale

    def get_handle(self):
        return self.ouser_handle

    def get_discriminator(self):
        return self.ouser_discriminator