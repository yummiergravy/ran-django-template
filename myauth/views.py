from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib import auth
from myauth.oauthlogic.exchange import exchange_code, fetchDiscordUserInfo
from myauth.models import OUser
import logging
from django.contrib import messages
import json

logger = logging.getLogger(__name__)
# Create your views here.


def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        return redirect('flash/logoutsuccessful')


def oauthexchange(request):
        authDICT = exchange_code(request.META['QUERY_STRING'])
        ouserdata = fetchDiscordUserInfo(authDICT["access_token"])
        
        try:#if user exists already, just log them in
            OUser.objects.get(username=ouserdata["id"])
            ouser = auth.authenticate(username=ouserdata["id"], password='pepperonidogfart')
            auth.login(request, ouser)
            return redirect('flash/loginsuccessful')

        except OUser.DoesNotExist:#if user does not exist, create account
            if ouserdata["verified"] == True:
                OUser.objects.create_user(username=ouserdata["id"], password='pepperonidogfart',email=ouserdata["email"], ouser_id=ouserdata["id"], \
                ouser_avatar="https://cdn.discordapp.com/" + ouserdata["id"] + "/" + ouserdata["avatar"] + ".png", \
                ouser_locale=ouserdata["locale"],ouser_handle=ouserdata["username"],ouser_discriminator=ouserdata["discriminator"])
                ouser = auth.authenticate(username=ouserdata["id"], password='pepperonidogfart')
                auth.login(request, ouser)
                return redirect('flash/signupsuccessful')
            else:
                return redirect('flash/discordnotver')










 