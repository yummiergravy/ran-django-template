from django.urls import path
from . import views


urlpatterns = [
    path('logout/', views.logout, name='LogoutPage'),
    #oauth stuff
    path('discordOauth2/', views.oauthexchange, name='oauthexchange'),



]
