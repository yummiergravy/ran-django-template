from Blog.settings import API_ENDPOINT, CLIENT_ID, CLIENT_SECRET, REDIRECT_URI
import requests
import sys

def exchange_code(utcode):
  code = utcode[5:]
  data = {
    'client_id': CLIENT_ID,
    'client_secret': CLIENT_SECRET,
    'grant_type': 'authorization_code',
    'code': code,
    'redirect_uri': REDIRECT_URI,
    'scope': 'identify email'
  }
  headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
  r = requests.post('%s/oauth2/token' % API_ENDPOINT, data=data, headers=headers)
  r.raise_for_status()
  return r.json()

def fetchDiscordUserInfo (token):
  authheaders = {
    "Authorization": "Bearer %s" % token
  }
  
  usrdta = requests.get('http://discordapp.com/api/users/@me', headers=authheaders)
  usrdta.raise_for_status()
  return usrdta.json() 